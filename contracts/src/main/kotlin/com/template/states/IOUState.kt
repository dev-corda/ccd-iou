package com.template.states

import com.template.contracts.IOUContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.ContractState
import net.corda.core.identity.Party
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState

// *********
// * State *
// *********
@BelongsToContract(IOUContract::class)

// Replace TemplateState's definition with:
class IOUState(val value: Int,
               val lender: Party,
               val borrower: Party) : ContractState{
    override val participants get() = listOf(lender, borrower)

}